version: '2'

services:
  social-web:
    image: iicc/social-web:${social_web_version}
    environment:
      AWARDS_API_BASE_URL: ${awards_api_base_url}
      BASE_URL: ${social_web_base_url}
      OAUTH_API_BASE_URL: ${oauth_api_base_url}
      SOCIAL_API_BASE_URL: ${social_api_base_url}
{{- if (.Values.social_public_port)}}
    ports:
      - ${social_public_port}:6201
{{- end}}
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.container.pull_image: always
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name={{.Stack.Name}}/social-web

  social-api:
    image: iicc/social-api:${social_api_version}
    environment:
      AWARDS_API_BASE_URL: ${awards_api_base_url}
      BASE_URL: ${social_api_base_url}
      OAUTH_API_BASE_URL: ${oauth_api_base_url}
{{- if (.Values.mongo_url)}}
      DB_URL: ${mongo_url}
{{- else}}
      DB_HOST: mongo
      DB_NAME: ${social_api_mongo_database}
{{- if (.Values.mongo_host)}}
      DB_PASSWORD: ${mongo_password}
      DB_USER: ${mongo_user}
{{- end}}
{{- end}}
{{- if (.Values.social_api_public_port)}}
    ports:
      - ${social_api_public_port}:6200
{{- end}}
{{- if (.Values.mongo_host)}}
    external_links:
      - ${mongo_host}:mongo
{{- else}}
    links:
      - mongo:mongo
{{- end}}
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.container.pull_image: always
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name={{.Stack.Name}}/social-api

  awards-api:
    image: iicc/awards-api:${awards_api_version}
    environment:
      BASE_URL: ${awards_api_base_url}
      OAUTH_API_BASE_URL: ${oauth_api_base_url}
{{- if (.Values.mongo_url)}}
      DB_URL: ${mongo_url}
{{- else}}
      DB_HOST: mongo
      DB_NAME: ${awards_api_mongo_database}
{{- if (.Values.mongo_host)}}
      DB_PASSWORD: ${mongo_password}
      DB_USER: ${mongo_user}
{{- end}}
{{- end}}
{{- if (.Values.awards_api_public_port)}}
    ports:
      - ${awards_api_public_port}:6202
{{- end}}
{{- if (.Values.mongo_host)}}
    external_links:
      - ${mongo_host}:mongo
{{- else}}
    links:
      - mongo:mongo
{{- end}}
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.container.pull_image: always
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name={{.Stack.Name}}/awards-api

{{- if not (.Values.mongo_host)}}
  mongo:
    image: mongo:3.0.15
    volume_driver: ${volume_driver}
    volumes:
      - {{.Stack.Name}}--mongo-data:/data/db
    labels:
      io.rancher.container.hostname_override: container_name
{{- end}}
