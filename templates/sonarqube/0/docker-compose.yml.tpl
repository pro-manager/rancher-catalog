version: '2'

services:
  sonarqube:
    image: sonarqube:${version}
    environment:
      SERVICE_VOLUME: /opt/sonarqube/extensions/plugins
      SONARQUBE_JDBC_PASSWORD: ${postgres_password}
      SONARQUBE_JDBC_USERNAME: ${postgres_user}
      SONARQUBE_WEB_JVM_OPTS: ${jvm_opts}
      SONARQUBE_JDBC_URL: jdbc:postgresql://postgres:${postgres_port}/${postgres_database}
    volume_driver: ${volume_driver}
    volumes:
      - {{.Stack.Name}}--sonarqube-files:/opt/sonarqube/extensions/plugins
{{- if (.Values.public_port)}}
    ports:
      - ${public_port}:9000
{{- end}}
{{- if (.Values.postgres_host)}}
    external_links:
      - ${postgres_host}:postgres
{{- else}}
    links:
      - postgres:postgres
{{- end}}
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name={{.Stack.Name}}/rocketchat

{{- if not (.Values.postgres_host)}}
  postgres:
    image: postgres:9.6.3-alpine
    environment:
      POSTGRES_USER: ${postgres_user}
      POSTGRES_PASSWORD: ${postgres_password}
      POSTGRES_DB: ${postgres_db}
    volume_driver: ${volume_driver}
    volumes:
      - {{.Stack.Name}}--postgres-data:/var/lib/postgresql
    labels:
      io.rancher.container.hostname_override: container_name
{{- end}}
