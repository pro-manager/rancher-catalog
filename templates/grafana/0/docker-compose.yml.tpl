version: '2'

services:
  prometheus:
    image: prom/prometheus:${prometheus_version}
    command: --config.file=/etc/prom-conf/prometheus.yml --storage.tsdb.path=/prometheus --web.console.libraries=/etc/prometheus/console_libraries --web.console.templates=/etc/prometheus/consoles
    user: root
    extra_hosts:
      - rancher-server:${rancher_server_ip}
    links:
      - cadvisor:cadvisor
      - node-exporter:node-exporter
      - prometheus-rancher-exporter:prometheus-rancher-exporter
    volumes_from:
      - prom-conf
    volume_driver: ${volume_driver}
    volumes:
      - {{.Stack.Name}}--prometheus-data:/prometheus
{{- if (.Values.prometheus_public_port)}}
    ports:
      - ${prometheus_public_port}:9090
{{- end}}
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.sidekicks: prom-conf

  grafana:
    image: grafana/grafana:${grafana_version}
    links:
      - prometheus:prometheus
      - prometheus-rancher-exporter:prometheus-rancher-exporter
    environment:
      GF_USERS_ALLOW_SIGN_UP: ${open_registration}
    volumes_from:
      - graf-db
{{- if (.Values.grafana_public_port)}}
    ports:
      - ${grafana_public_port}:3000
{{- end}}
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.sidekicks: graf-db

  node-exporter:
    image: prom/node-exporter:latest
    tty: true
    stdin_open: true
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.global: true

  prometheus-rancher-exporter:
    image: infinityworks/prometheus-rancher-exporter:v0.22.52
    labels:
      io.rancher.container.agent.role: environment
      io.rancher.container.create_agent: true
      io.rancher.container.hostname_override: container_name

  cadvisor:
    image: google/cadvisor:latest
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker:/var/lib/docker:ro
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.global: true

  prom-conf:
    image: infinityworks/prom-conf:20
    network_mode: none
    tty: true
    volumes:
      - /etc/prom-conf
    labels:
      io.rancher.container.hostname_override: container_name

  graf-db:
    image: codejamninja/graf-db:latest
    command: cat
    network_mode: none
    volume_driver: ${volume_driver}
    volumes:
      - {{.Stack.Name}}--graf-db-data:/var/lib/grafana
    labels:
      io.rancher.container.hostname_override: container_name
