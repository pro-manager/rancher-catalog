# grafana

> data visualization and monitoring with prometheus integration


## Ports

### prometheus: `9090`
### grafana: `3000`


## Usage

### Rancher
Follow the instructions [HERE](https://github.com/infinityworks/Guide_Rancher_Monitoring) to setup Rancher correctly
