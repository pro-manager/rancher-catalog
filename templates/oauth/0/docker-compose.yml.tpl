version: '2'

services:
  oauth-api:
    image: iicc/oauth-api:${version}
    environment:
      AWS_CLIENT_ID: ${aws_client_id}
      AWS_CLIENT_SECRET: ${aws_client_secret}
      AWS_REGION: ${aws_region}
      BASE_URL: ${base_url}
      EMAIL_FROM: ${email_from}
      FACEBOOK_CLIENT_ID: ${facebook_client_id}
      FACEBOOK_CLIENT_SECRET: ${facebook_client_secret}
      GITHUB_CLIENT_ID: ${github_client_id}
      GITHUB_CLIENT_SECRET: ${github_client_secret}
      GOOGLE_CLIENT_ID: ${google_client_id}
      GOOGLE_CLIENT_SECRET: ${google_client_secret}
      INSTAGRAM_CLIENT_ID: ${instagram_client_id}
      INSTAGRAM_CLIENT_SECRET: ${instagram_client_secret}
      LINKEDIN_CLIENT_ID: ${linkedin_client_id}
      LINKEDIN_CLIENT_SECRET: ${linkedin_client_secret}
      SECRET: ${secret}
      SMTP_HOST: ${smtp_host}
      SMTP_PASSWORD: ${smtp_password}
      SMTP_PORT: ${smpt_port}
      SMTP_SECURE: ${smpt_secure}
      SMTP_USERNAME: ${smtp_username}
      TWILIO_NUMBER: ${twilio_number}
      TWILIO_SID: ${twilio_sid}
      TWILIO_TOKEN: ${twilio_token}
      TWITTER_CLIENT_ID: ${twitter_client_id}
      TWITTER_CLIENT_SECRET: ${twitter_client_secret}
{{- if (.Values.mongo_url)}}
      DB_URL: ${mongo_url}
{{- else}}
      DB_HOST: mongo
      DB_NAME: ${mongo_database}
{{- if (.Values.mongo_host)}}
      DB_PASSWORD: ${mongo_password}
      DB_USER: ${mongo_user}
{{- end}}
{{- end}}
{{- if (.Values.public_port)}}
    ports:
      - ${public_port}:6100
{{- end}}
{{- if (.Values.mongo_host)}}
    external_links:
      - ${mongo_host}:mongo
{{- else}}
    links:
      - mongo:mongo
{{- end}}
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.container.pull_image: always
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name={{.Stack.Name}}/oauth-api

{{- if not (.Values.mongo_host)}}
  mongo:
    image: mongo:3.0.15
    environment:
      MONGO_DATABASE: ${mongo_database}
    volume_driver: ${volume_driver}
    volumes:
      - {{.Stack.Name}}--mongo-data:/data/db
    labels:
      io.rancher.container.hostname_override: container_name
{{- end}}
