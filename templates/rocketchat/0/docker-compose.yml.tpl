version: '2'

services:
  rocketchat:
    image: rocketchat/rocket.chat:${version}
    volume_driver: ${volume_driver}
    volumes:
      - {{.Stack.Name}}--rocketchat-files:/app/uploads
    environment:
      PORT: 3000
      ROOT_URL: ${base_url}
{{- if (.Values.mongo_host)}}
      MONGO_URL: mongodb://${mongo_user}:${mongo_password}@mongo:${mongo_port}/${mongo_database}
{{- else}}
      MONGO_URL: mongodb://mongo:${mongo_port}/${mongo_database}
{{- end}}
{{- if (.Values.public_port)}}
    ports:
      - ${public_port}:3000
{{- end}}
{{- if (.Values.mongo_host)}}
    external_links:
      - ${mongo_host}:mongo
{{- else}}
    links:
      - mongo:mongo
{{- end}}
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name={{.Stack.Name}}/rocketchat

{{- if not (.Values.mongo_host)}}
  mongo:
    image: mongo:3.0.15
    environment:
      MONGO_DATABASE: ${mongo_database}
    volume_driver: ${volume_driver}
    volumes:
      - {{.Stack.Name}}--mongo-data:/data/db
    labels:
      io.rancher.container.hostname_override: container_name
{{- end}}
